const bytes = require('bytes')
const aws_sdk = require('aws-sdk')
const match = require('minimatch')
const crypto = require('crypto')
const md5 = v=>crypto.createHash('md5').update(v).digest('hex')

async function main(){
    let s3 = new aws_sdk.S3()
    let [node, self, src, dest] = process.argv
    let src_url = new URL(src)
    let src_bucket = src_url.hostname
    let dest_bucket = src_url.hostname
    let pattern = src_url.pathname.slice(1)
    let parts = src_url.pathname.split('/')
    let prefix = parts.slice(0, -1).join('/')
    let remote_files = [];
    let token
    do {
        let res = await s3.listObjectsV2({
            Bucket: src_bucket,
            Prefix: prefix.slice(1),
            ContinuationToken: token
        }).promise()
        remote_files = remote_files.concat(res.Contents.map(f=>{
            return {
                key: f.Key,
                size: f.Size,
            }
        }))
        token = res.NextContinuationToken
    } while (token)
    let src_files = remote_files.filter(f=>match(f.key, pattern)).sort((a, b)=>b.size-a.size)
    let upload =  await s3.createMultipartUpload({
        Bucket: dest_bucket,
        Key: dest,
    }).promise()
    let upload_id = upload.UploadId
    console.error(`Building file key=${dest} upload_id=${upload_id} from ${src_files.length} parts`)
    let tasks = Object.entries(src_files).map(([i, f])=>{
        let number = +i+1
        return {
            number,
            src_object: `${src_bucket}/${f.key}`,
            size: f.size,
        }
    }).reduce((o, task)=>{
        o.set(task.number, task)
        return o
    }, new Map())
    let parallel = +process.env.PARALLEL||30
    await map_limit(parallel, [...tasks.values()].reverse(), async t=>{
        console.error(`Writing part=${t.number} key=${t.src_object} size=${bytes(t.size)} of ${dest}`)
        let opt = {
            Bucket:  dest_bucket,
            Key: dest,
            CopySource: t.src_object,
            PartNumber: t.number,
            UploadId: upload_id,
        }
        let start = Date.now()
        let res = await s3.uploadPartCopy(opt).promise()
        t.etag = res.CopyPartResult.ETag;
        let ms = Date.now()-start
        console.error(`Done part=${t.number} key=${t.src_object} of ${dest} in ${ms}ms`)
    })
    await s3.completeMultipartUpload({
        Bucket: dest_bucket,
        Key: dest,
        UploadId: upload_id,
        MultipartUpload: {
            Parts: [...tasks.values()].map(t=>{
                return {PartNumber: t.number, ETag: t.etag}
            }),
        },
    }).promise()
}

const sleep = ms=>new Promise(resolve=>setTimeout(resolve, ms))

const map_limit = async (n, array, fn)=>{
    let tasks = array.reverse()
    let launch = async ()=>{
        while (tasks.length)
            await fn(tasks.pop())
    }
    let workers = []
    for (let i = 0; i<n; i++)
        workers.push(launch())
    return await Promise.all(workers)
}


if (!module.parent)
    main().catch(e=>console.error(e))
