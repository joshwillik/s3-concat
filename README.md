# s3-concat

## Origin

Inspired by https://github.com/whitfin/s3-utils.

Opened as a path around https://github.com/whitfin/s3-utils/issues/3 

## Usage

node main.js s3://bucket/path-to-files/<glob-pattern> <dest_file>

## Known problems (pull requests welcome)

- Multiple files <5mb will cause the file merge to fail (this tool should compact them ahead of the merge or merge in memory)
- Weird CLI usage (clone + run in dir)
- No option to merge into a new bucket
